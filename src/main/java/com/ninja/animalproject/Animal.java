/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ninja.animalproject;

/**
 *
 * @author USER
 */
public class Animal {
    
    protected String name;
    protected int numberOfLegs = 0;
    protected String color;

    public Animal(String name, String color ,int numlags) {
        System.out.println("        Animal created");
        this.name = name;
        this.color = color;
        this.numberOfLegs = numlags ;
}
        public void walk() {
        System.out.println("Animal walk");
    }

    public void speak() {
        System.out.println("Animal speak");
        System.out.println("My name : " + this.name + "  My color : " 
                + this.color + "  My lags : " + this.numberOfLegs);
    }
    public void end(){
        System.out.println("---------------------------------------"
                + "-----------------------------------------------");
    }

    public String getName() {
        return name;
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public String getColor() {
        return color;
    }

}

