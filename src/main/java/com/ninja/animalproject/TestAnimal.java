/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ninja.animalproject;

/**
 *
 * @author USER
 */
public class TestAnimal {

    public static void main(String[] args) {
        Animal animal = new Animal("Ani", "white", 0);
        animal.walk();
        animal.speak();
        animal.end();

        Dog dhang = new Dog("Dhang", "Black");
        dhang.speak();
        dhang.walk();
        System.out.println("Dhang is Animal: " + (dhang instanceof Animal));
        System.out.println("Dhang is Dog: " + (dhang instanceof Dog));
        dhang.end();

        Dog kaow = new Dog("Kaow", "Brown");
        kaow.speak();
        kaow.walk();
        System.out.println("Kaow is Animal: " + (kaow instanceof Animal));
        System.out.println("Kaow is Dog: " + (kaow instanceof Dog));
        kaow.end();

        Dog boo = new Dog("Boo", "Black & White");
        boo.speak();
        boo.walk();
        System.out.println("Boo is Animal: " + (boo instanceof Animal));
        System.out.println("Boo is Dog: " + (boo instanceof Dog));
        boo.end();

        Dog taw = new Dog("Taw", "Black & White");
        taw.speak();
        taw.walk();
        System.out.println("Taw is Animal: " + (taw instanceof Animal));
        System.out.println("Taw is Dog: " + (taw instanceof Dog));
        taw.end();

        Cat kai = new Cat("Kai", "Brown");
        kai.speak();
        kai.walk();
        System.out.println("Kai is Animal: " + (kai instanceof Animal));
        System.out.println("Kai is Cat: " + (kai instanceof Cat));
        kai.end();

        Duck somsak = new Duck("SomSak", "Grey");
        somsak.speak();
        somsak.walk();
        somsak.fly();
        System.out.println("SomSak is Animal: " + (somsak instanceof Animal));
        System.out.println("SomSak is Duck: " + (somsak instanceof Duck));
        somsak.end();

        Duck gho = new Duck("Gho", "Grey");
        gho.speak();
        gho.walk();
        gho.fly();
        System.out.println("Gho is Animal: " + (somsak instanceof Animal));
        System.out.println("Gho is Duck: " + (gho instanceof Duck));
        gho.end();

        Animal[] animals = {dhang, kaow, boo, taw, kai, somsak, gho};
        for (int i = 0; i < animals.length; i++) {
            System.out.println("            Speak & Walk");
            animals[i].speak();
            animals[i].walk();

            if (animals[i] instanceof Duck) {
                Duck duck = (Duck) animals[i];
                duck.fly();
            }
            animals[i].end();

        }

    }

}

